# Django on Debian and derivatives
# Notes about id fields and wisdom (or otherwise) of using filepaths as id
# ID: Must be unique across entire state tree.
#     If the same ID declaration is used twice,
#     only the first one matched will be used...
#     ... All subsequent ID declarations with the same name will be ignored
# YAML and repeated keys:
#   dictionary keys need to be unique so within a given block if you redefine a key, then the last one wins out
{% set django_www = salt['pillar.get']('django_www_filepath','/var/www/django') %}
django_packages:
  pkg.installed:
    - pkgs:
      - python-django
      - python-django-djapian
      - python-pygments
      - python-yaml

django_wsgi_module:
  pkg.installed:
    - pkgs:
      - libapache2-mod-wsgi
  service:
    - name: apache2
    - running
    - enable: True

#django_index_html:
#  file.managed:
#    - name: {{ django_www }}/index.html
#    - source: salt://django/index.html


django_settings_py:
  file.managed:
    - name: {{ django_www }}/settings.py
    - contents: |
        DEBUG = False
        ROOT_URLCONF = 'urls'
        TEMPLATE_DIRS = TEMPLATE_DIRS = [ '{{ django_www }}/templates' ]
    #    TEMPLATE_DIRS = TEMPLATE_DIRS = [ '{{ django_www }}' ]
    #    TEMPLATE_DIRS = ['.'
    #    TEMPLATE_DIRS = TEMPLATE_DIRS = [ BASE_DIR, '{{ django_www }}' ]]
    # - source: salt://django/settings.py
    - backup: minion


django_urls_py:
  file.managed:
    - name: {{ django_www }}/urls.py
    - contents: |
        from django.conf.urls import patterns, url, include
        from django.views import generic as views_generic
        from django.views.defaults import page_not_found as view_page_not_found
        from django.views.defaults import server_error as view_server_error

        urlpatterns = patterns('',
          (r'^about/', views_generic.TemplateView.as_view(template_name="about.html")),
          (r'^401/$', views_generic.TemplateView.as_view(template_name="unauth.html")),
          (r'^404/$', view_page_not_found),
          (r'^500/$', view_server_error),
        )	
    # - source: salt://django/urls.py
    - backup: minion


django_wsgi_py:
  file.managed:
    - name: {{ django_www }}/wsgi.py
    - contents: |
        # WSGI config for djlite project.
        from os import environ

        environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

        # This application object is used by any WSGI server configured to use this
        # file. This includes Django's development server, if the WSGI_APPLICATION
        # setting points here.
        
        from django.core.wsgi import get_wsgi_application
        application = get_wsgi_application()
    # - source: salt://django/wsgi.py
    - backup: minion


django_wsgi_conf:
  file.append:
    - name: /etc/apache2/mods-available/wsgi.conf
    - text: |
        <IfModule mod_wsgi.c>
        WSGIPythonPath {{ django_www }}
        </IfModule>
    - backup: minion
    - require:
      - pkg: django_wsgi_module
    - watch_in:
      - service: apache2


django_wsgi_site_available:
  file.managed:
    - name: /etc/apache2/sites-available/djlite
    - require_in:
      - file: /etc/apache2/sites-enabled/djlite
    - contents: |
        <VirtualHost *:80>

        DocumentRoot {{ django_www }}

        <Directory "{{ django_www }}/">
           #Options -Indexes
           AllowOverride None
           Order allow,deny
           allow from all
        </Directory>

           WSGIDaemonProcess djlite user=www-data group=www-data processes=1 threads=2
           WSGIScriptAlias /djlite {{ django_www }}/wsgi.py
           #WSGIPythonPath {{ django_www }}/ # wsgi.conf
 
           Alias /static /var/www/html
        </VirtualHost>

django_wsgi_site_enabled:
  file.symlink:
    - name: /etc/apache2/sites-enabled/djlite
    - target: /etc/apache2/sites-available/djlite
    - force: True
    - watch_in:
      - service: apache2
    - require:
      - file: /etc/apache2/sites-enabled/000-default 


apache2default_disable:
  cmd.run:
    - name: '/usr/sbin/a2dissite 000-default'
    #- prereq:
    #  - file: django_wsgi_site_enabled


apache2default_remove:
  file.absent:
    - name: /etc/apache2/sites-enabled/000-default


django401template:
  file.managed:
    - name: {{ django_www }}/templates/unauth.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>No authority</title>
        </head>
        <body>
        <p>Unable to access the requested page - no authority.</p>
        </body>
        <!-- http 401 error -->
        </html>
    - backup: minion


django404template:
  file.managed:
    - name: {{ django_www }}/templates/404.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>Page not found</title>
        </head>
        <body>
        <p>Unable to find the requested page - not found.</p>
        </body>
        <!-- http 404 error -->
        </html>
    # - source: salt://django/urls.py
    - backup: minion


django500template:
  file.managed:
    - name: {{ django_www }}/templates/500.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>Page error</title>
        </head>
        <body>
        <p>Unable to process the requested page - malfunction.</p>
        </body>
        <!-- http 500 error -->
        </html>
    - backup: minion


django_about_template:
  file.managed:
    - name: {{ django_www }}/templates/about.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>About Page - djlite</title>
        </head>
        <body>
        <p>djlite application from automation</p>
        </body>
        </html>
    - backup: minion



