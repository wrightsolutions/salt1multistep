# encfs on Debian 8 jessie
# Notes about id fields and wisdom (or otherwise) of using filepaths as id
# ID: Must be unique across entire state tree.
#     If the same ID declaration is used twice,
#     only the first one matched will be used...
#     ... All subsequent ID declarations with the same name will be ignored
# YAML and repeated keys:
#   dictionary keys need to be unique so within a given block if you redefine a key, then the last one wins out
# YAML lists and ordering:
#   YAML lists should preserve order, but how the consumer application (Salt) deals with that for cmd.run names
#   should be verified if you are planning to run multiline cmd using  '- names:' form of cmd.run
#

modinfo_packages:
  pkg.installed:
    - pkgs:
      - kmod


encfs_packages:
  pkg.installed:
    - pkgs:
      - ecryptfs-utils
      - encfs
      - libpam-encfs
      - libpam-mount


encfs_modinfo:
  cmd.run:
    - names:
      - /sbin/modinfo ecryptfs
      - /sbin/lsmod
    # Only use multiple entries in names of cmd.run when ok to be run in any order
    - timeout: 30
    - require:
      - pkg: modinfo_packages
      - pkg: encfs_packages


#encfs_modules_conf_append:
#  file.append:
#    - name: /etc/modules-load.d/modules.conf
#    - text: |
#        ecryptfs
#    # - source: salt://fein/modules.conf
#    - backup: minion
#    - require:
#      - pkg: encfs_packages


encfs_modules_conf:
  file.managed:
    - name: /etc/modules-load.d/modules.conf
    - contents: |
        # /etc/modules: kernel modules to load at boot time.
        #
        # This file contains the names of kernel modules that should be loaded
        # at boot time, one per line. Lines beginning with "#" are ignored.

        ecryptfs
    # - source: salt://fein/modules.conf
    - backup: minion
    - require:
      - pkg: encfs_packages


encfs_homes_list:
  cmd.run:
    - name: >
       find /home/.encfs/ -type f
       -name '.encfs6.xml' -mtime -7 -ls
    - timeout: 40
    #- prereq:
    #  - file: fein_db_sqlite
    - require:
      - pkg: encfs_packages


#encfs_no_idle:
#  file.sed:
#    - name: /etc/security/pam_encfs.conf
#    - before: 'default --idle=1'
#    - after: 'default'
#    - limit: ^encfs_default --idle


encfs_no_idle:
  file.managed:
    - name: /etc/security/pam_encfs.conf
    - contents: |
        #This file is parsed top to bottom, until the first mount line that matches is found, then it stops.

        #Note that I dont support spaces in params
        #So if your for example gonna specify idle time use --idle=X not -i X.

        #If this is specified program will attempt to drop permissions before running encfs. 
        #(will not work with --public for example, as that requires encfs to run as root)
        drop_permissions

        #This specifies which options to pass to encfs for every user.
        #You can find encfs options by running encfs without any arguments
        encfs_default

        #Same for fuse, note that allow_root (or allow_other, or --public in encfs) is needed to run gdm/X.
        #you can find fuse options with encfs -H
        # - Debian Note:
        # allow_other and allow_root are mutually incompatible and cannot be set 
        # simultaneously. If we set any of them here (as done before) all users 
        # inherit it and the other option cannot be set for any user. We better 
        # do not set it here, but in specific definitions at the end of this file.
        fuse_default nonempty

        #For a mount line, - = generic, we try to fill in what we need.
        #A Mount line is constructed like this:
        #USERNAME		if "-" or "*" gets replaced with $USER
        #SOURCE			if USERNAME is -, replace with path + /$USER
        #				if USERNAME is *, replace with $HOME/ + sourcepath
        #TARGET PATH	if - replace with $HOME
        #				if USERNAME is *, replace with $HOME/ + targetpath
        #ENCFS OPTIONS	encfs options here is encfs_default + encfs_options
        #FUSE OPTIONS	encfs options here is fuse_default + fuse_options

        #Keep in mind that the configuration file is parsed top to bottom, so if you put your generic line on top, 
        #that will always match before any custom lines under it.


        #In this example, with example_user uncommented, the "-" line will never be parsed if you login as example_user.
        #In the lines with the USERNAME "*", all paths are relative to $HOME
        #USERNAME    	SOURCE 			TARGET PATH      	ENCFS Options		FUSE Options
        #example_user	/mnt/enc/example_user	/home/example_user	-v,--idle=1		allow_root
        #*		.private		private			-v			allow_other
        #-		/mnt/enc		- 			-v			allow_other
        -		/home/.enc		- 			-v			allow_root
    - backup: minion
    - require:
      - pkg: encfs_packages


encfs_ice:
  file.append:
    - name: /etc/security/pam_env.conf
    - text: |
        #
        # set the ICEAUTHORITY file location to allow GNOME to start on encfs $HOME
        ICEAUTHORITY DEFAULT=/tmp/.ICEauthority_@{PAM_USER}
    - require:
      - pkg: encfs_packages


encfs_session:
  file.managed:
    - name: /etc/pam.d/common-session
    - contents: |
        #
        # /etc/pam.d/common-session - session-related modules common to all services
        #
        # This file is included from other service-specific PAM config files,
        # and should contain a list of modules that define tasks to be performed
        # at the start and end of sessions of *any* kind (both interactive and
        # non-interactive).
        #
        # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
        # To take advantage of this, it is recommended that you configure any
        # local modules either before or after the default block, and use
        # pam-auth-update to manage selection of other modules.  See
        # pam-auth-update(8) for details.

        # here are the per-package modules (the "Primary" block)
        session	[default=1]			pam_permit.so
        # here's the fallback if no module succeeds
        session	requisite			pam_deny.so
        # prime the stack with a positive return value if there isn't one already;
        # this avoids us returning an error just because nothing sets a success code
        # since the modules above will each just jump around
        session	required			pam_permit.so
        # and here are more per-package modules (the "Additional" block)
        session	required	pam_encfs.so
        session	required	pam_unix.so
        session	optional	pam_mount.so 
        session	optional	pam_ck_connector.so nox11
        session	optional	pam_ecryptfs.so unwrap
        # end of pam-auth-update config
    - backup: minion
    - require:
      - pkg: encfs_packages


encfs_mount_verify:
  cmd.run:
    - names:
      - /bin/mount
      - /usr/bin/whoami
    # Only use multiple entries in names of cmd.run when ok to be run in any order
    - onlyif: /bin/fgrep -qs 'fuse.encfs' /proc/mounts
    - order: last

