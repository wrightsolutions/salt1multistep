# File naming - init.sls or not.

This directory will contain several examples, and it
is impractical to expect them all to be named init.sls.

So we use arbitrary names for storing sls in this version control

See [Salt own naming conventions for .sls](http://docs.saltstack.com/en/latest/topics/tutorials/states_pt1.html#sls-file-namespace)
for guidance on how / whether to rename these files as you make use of them in real setups.

Some sls will attempt to automatically install mercurial if not present.

# 'simple' or 'multistep'

Simple or starter scripts will be named program1simple.sls  
or some other variant with simple in the filename.

More complex examples will be program1multistep.sls  
or some other variant with multistep in the filename.

If you find that locally a multstep script does not complete,
then consider the 'simple' variant and diff later.


# Example of using salt-ssh to use local .sls recipe to issue commands on remote server

Here I am assuming you are root and have already issued  
```ssh-copy-id -i /etc/salt/pki/master/ssh/salt-ssh.rsa.pub aa.bb.cc.dd```

+ mkdir /srv/salt/encfs
+ cp -p encfs1simple.sls /srv/salt/encfs/init.sls
+ printf 'scratch8:\n  host: aa.bb.cc.dd\n' >> /etc/salt/roster
+ salt-ssh 'scratch8' state.sls encfs

