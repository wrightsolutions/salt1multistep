# Django Fein on Debian and derivatives
# Notes about id fields and wisdom (or otherwise) of using filepaths as id
# ID: Must be unique across entire state tree.
#     If the same ID declaration is used twice,
#     only the first one matched will be used...
#     ... All subsequent ID declarations with the same name will be ignored
# YAML and repeated keys:
#   dictionary keys need to be unique so within a given block if you redefine a key, then the last one wins out
# YAML lists and ordering:
#   YAML lists should preserve order, but how the consumer application (Salt) deals with that for cmd.run names
#   should be verified if you are planning to run multiline cmd using  '- names:' form of cmd.run
#
# Assuming you have a local port redirected vm sending 8080 to 80 of vm then visit these urls:
#  http://127.0.0.1:8080/cms
#  http://127.0.0.1:8080/st/coming_soon.html
#  http://127.0.0.1:8080/cms/page_counts
#  http://127.0.0.1:8080/cms/lettuce
#
{% set fein_www = salt['pillar.get']('fein_www_filepath','/var/www/fein') %}
{% set wwwgroup = salt['pillar.get']('fein_wwwgroup','www-data') %}
{% set adminuser = salt['pillar.get']('fein_adminuser','admin') %}
{% set adminpassword = salt['pillar.get']('fein_adminpassword','fein2014') %}
{% set appname = 'cmsapp' %}
fein_packages:
  pkg.installed:
    - pkgs:
      - python-django
      - python-django-feincms
      - python-django-tinymce
      - python-django-debug-toolbar
      - sqlite3
      # sqlite3 for /usr/bin/django-admin dbshell
      - python-pygments
      - python-yaml
      # mysql-server-5.5
      # python-mysqldb
      # postgresql
      # python-psycopg2
      - mercurial
      - tinymce

fein_wsgi_module:
  pkg.installed:
    - pkgs:
      - libapache2-mod-wsgi
  service:
    - name: apache2
    - running
    - enable: True


#fein_wsgi_module_enable:
  #apache_module.enable:
  #- name: wsgi

#fein_index_html:
#  file.managed:
#    - name: {{ fein_www }}/index.html
#    - source: salt://fein/index.html


fein_www_directory:
  file.directory:
    - name: {{ fein_www }}/
    - user: root
    - group: {{ wwwgroup }}
    - mode: 2750
    - clean: True
    - exclude_pat: '*.py'


fein_settings_py:
  file.managed:
    - name: {{ fein_www }}/settings.py
    - contents: |
        # ADMINS ; MANAGERS ;
        # INTERNAL_IPS ; ALLOWED_HOSTS ;
        # INTERNAL_IPS = () is a tuple of IP addresses, as strings, that
        # See debug comments, when DEBUG is true and also receive x-headers
        INTERNAL_IPS = ('127.0.0.1', '10.0.0.1',)
        # Hosts/domain names that are valid for this site. Django 1.5 default is ALLOWED_HOSTS = []
        # "*" matches anything, ".example.com" matches example.com and all subdomains
        ALLOWED_HOSTS = ()
        import os.path; BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        # Above BASE_DIR is a local definition rather than a Django built in variable
        DATABASES = {
          'default': {
             'ENGINE': 'django.db.backends.sqlite3',             
             'NAME': '{{ fein_www }}_data/fein16.sqlite',
             # 'ENGINE': 'django.db.backends.dummy', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
             # 'NAME': '',                           # Or path to database file if using sqlite3.
             # 'NAME': '{{ fein_www }}/fein16.sqlite',
             'HOST': '',                           # Set to empty string for localhost. Not used with sqlite3.
             'OPTIONS': {},
             'PASSWORD': u'********************',  # Not used with sqlite3.
             'PORT': '',                           # Set to empty string for default. Not used with sqlite3.
             'TEST_CHARSET': None,
             'TEST_COLLATION': None,
             'TEST_MIRROR': None,
             'TEST_NAME': None,
             'TIME_ZONE': 'UTC',
             'USER': ''                            # Not used with sqlite3.
          }
        }
        TIME_ZONE = 'Europe/London'
        LANGUAGE_CODE = 'en-gb'
        SITE_ID = 1
        # If USE_I18N = False, then Django will not load the internationalization machinery.
        USE_I18N = False
        USE_L10N = True
        USE_TZ = True
        APPNAME = '{{ appname }}'
        # Above APPNAME rather than APP_NAME as APPNAME never replaced - actual variable key in this file
        MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media/')
        MEDIA_URL = '/media/'
        ROOT_URLCONF = 'urls'
        STATIC_ROOT = ''
        STATIC_URL = 'static/'
        # STATIC_ROOT; STATIC_URL ; STATICFILES_DIRS ; STATICFILES_FINDERS ; SECRET_KEY 
        TEMPLATE_DIRS = [ '{{ fein_www }}/templates' ]
        try:
            import django.conf.global_settings as DEFAULT_SETTINGS
        except ImportError:
            print 'Unable to import global_settings as DEFAULT_SETTINGS'
        TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
          'django.contrib.auth.context_processors.auth',
          'django.contrib.messages.context_processors.messages',
          'django.core.context_processors.request',
        )
        # TEMPLATE_LOADERS
        MIDDLEWARE_CLASSES = DEFAULT_SETTINGS.MIDDLEWARE_CLASSES + (
          'django.middleware.clickjacking.XFrameOptionsMiddleware',
        )
        # Above we append a singleton to existing tuple.
        INSTALLED_APPS = DEFAULT_SETTINGS.INSTALLED_APPS + (
          'django.contrib.auth',
          'django.contrib.contenttypes',
          'django.contrib.sites',
          'django.contrib.sessions',
        )
        # Add '{{ appname }}' to INSTALLED_APPS above once you have done startapp {{ appname }}
        # WSGI_APPLICATION ; LOGGING
        try:
            from local_settings import *
        except ImportError:
            print 'local_settings.py deployment settings missing?:'
            """ By manually setting False on next two lines, we ensure
            that debug can only ever be enabled by a present and 
            correct local_settings.py """
            DEBUG = False
            TEMPLATE_DEBUG = DEBUG
    # - source: salt://fein/settings.py
    - backup: minion


fein_generate_secret_py:
  file.managed:
    - name: {{ fein_www }}/generate_secret.py
    - contents: |
        #!/usr/bin/env python
        # -*- coding: utf-8 -*-
        from re import sub as resub
        from string import ascii_letters, digits, punctuation
        punct_remove = "[{0}{1}{2}{2}]".format(chr(34),chr(39),chr(92))
        punct = resub(punct_remove, '', punctuation)
        seed_chars = "{0}{1}{2}".format(ascii_letters, digits, punct)
        from django.utils.crypto import get_random_string
        outfile_path = '{{ fein_www }}/local_settings.py'
        with open(outfile_path, 'wb') as outfile:
          try:
            outfile.write("SECRET_KEY = '{0}'\n".format(get_random_string(50, seed_chars)))
          except:
            pass
    - require:
      - file: fein_www_directory
      - file: fein_settings_py


fein_generate_secret_run:
  cmd.run:
    - name: /usr/bin/python2 {{ fein_www }}/generate_secret.py
    - timeout: 20
    - require:
      - file: fein_generate_secret_py


fein_local_settings_py:
  file.append:
    - name: {{ fein_www }}/local_settings.py
    - text: |
        DEBUG = False
        TEMPLATE_DEBUG = DEBUG
        try:
            import django.conf.global_settings as DEFAULT_SETTINGS
        except ImportError:
            print 'Local settings was unable to import global_settings as DEFAULT_SETTINGS'
        APPNAME = '{{ appname }}'
        # Above APPNAME rather than APP_NAME as APPNAME never replaced - actual variable key in this file
        # ADMINS ; MANAGERS ;
        # INTERNAL_IPS = () is a tuple of IP addresses, as strings, that
        # See debug comments, when DEBUG is true and also receive x-headers
        INTERNAL_IPS = ('127.0.0.1', '10.0.2.0',)
        # Hosts/domain names that are valid for this site. Django 1.5 default is ALLOWED_HOSTS = []
        # "*" matches anything, ".example.com" matches example.com and all subdomains
        ALLOWED_HOSTS = ('*')
        import os.path; LOCAL_SETTINGS_DIR = os.path.dirname(os.path.abspath(__file__))
        #DATABASES = {
        #  'default': {
        #     'ENGINE': 'django.db.backends.sqlite3',             
        #     'NAME': '{{ fein_www }}_data/fein16.sqlite',
        #     'HOST': '',                           # Set to empty string for localhost. Not used with sqlite3.
        #     'OPTIONS': {},
        #     'PASSWORD': u'********************',  # Not used with sqlite3.
        #     'PORT': '',                           # Set to empty string for default. Not used with sqlite3.
        #     'TEST_CHARSET': None,
        #     'TEST_COLLATION': None,
        #     'TEST_MIRROR': None,
        #     'TEST_NAME': None,
        #     'TIME_ZONE': 'UTC',
        #     'USER': ''                            # Not used with sqlite3.
        #  }
        #}
        # MEDIA_ROOT; MEDIA_URL ; ROOT_URLCONF
        # STATIC_ROOT; STATIC_URL ; STATICFILES_DIRS ; STATICFILES_FINDERS ; SECRET_KEY
        TEMPLATE_CONTEXT_PROCESSORS = (
          'django.contrib.auth.context_processors.auth',
          'django.core.context_processors.debug',
          'django.core.context_processors.i18n',
          'django.core.context_processors.media',
          'django.core.context_processors.request',
          'django.core.context_processors.static',

          'feincms.context_processors.add_page_if_missing',
        )

        TEMPLATE_LOADERS = (
          'django.template.loaders.filesystem.Loader',
          'django.template.loaders.app_directories.Loader',
        )

        MIDDLEWARE_CLASSES = DEFAULT_SETTINGS.MIDDLEWARE_CLASSES + (
          'debug_toolbar.middleware.DebugToolbarMiddleware',
          'django.middleware.common.CommonMiddleware',
          'django.contrib.sessions.middleware.SessionMiddleware',
          'django.middleware.csrf.CsrfViewMiddleware',
          'django.contrib.auth.middleware.AuthenticationMiddleware',
          'django.contrib.messages.middleware.MessageMiddleware',
          'django.middleware.clickjacking.XFrameOptionsMiddleware',
        )

        MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media/')
        MEDIA_URL = '/media/'
        #ROOT_URLCONF = 'urls'
        STATIC_ROOT = os.path.join(LOCAL_SETTINGS_DIR, 'static/')
        STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static/')
        STATIC_URL = "/static/".format(APPNAME)
        STATICFILES_DIRS = ('/usr/share/tinymce/',
        )

        TEMPLATE_DIRS = (
        )

        INSTALLED_APPS = DEFAULT_SETTINGS.INSTALLED_APPS + (
          'django.contrib.auth',
          'django.contrib.contenttypes',
          'django.contrib.sessions',
          'django.contrib.sites',

          'django.contrib.messages',
          'django.contrib.staticfiles',
          # Uncomment the next line to enable the admin:
          'django.contrib.admin',
          # Uncomment the next line to enable admin documentation:
          # 'django.contrib.admindocs',

          'debug_toolbar',

          'feincms',
          'feincms.module.page',
          'feincms.module.medialibrary',
          APPNAME,
          'mptt',
          'feincms.module.blog',
        )

        FEINCMS_RICHTEXT_INIT_CONTEXT = {
          'TINYMCE_JS_URL': STATIC_URL + 'www/tiny_mce.js',
        }

        if DEBUG:
            from fnmatch import fnmatch
            class glob_list(list):
                def __contains__(self, key):
                    # fnmatch.fnmatch(s, pat) Test whether the pattern pat matches
                    for pat in self:
                        if fnmatch(key, pat): return True
                    return False
            INTERNAL_IPS = glob_list(['127.0.0.1', '10.0.2.*', '10.0.0.*', '172.16.*.*'])
    - require:
      - file: fein_data_directory
      - file: fein_generate_secret_py


fein_generate_secret_remove:
  file.absent:
    - name: {{ fein_www }}/generate_secret.py
    - require:
      - file: fein_local_settings_py


fein_local_settings_py_backup:
  file.copy:
    - name: {{ fein_www }}/local_settings_backup.py
    - source: {{ fein_www }}/local_settings.py
    - require:
      - file: fein_local_settings_py
    - backup: minion


fein_data_directory:
  file.directory:
    - name: {{ fein_www }}_data
    - user: root
    - group: {{ wwwgroup }}
    - mode: 2775
    - clean: True
    - exclude_pat: '*.sqlite'


#fein_db_page_page_export:
#  cmd.run:
#    - name: >
#       /usr/bin/sqlite3 {{ fein_www }}_data/fein16.sqlite ".dump page_page"
#       | /bin/egrep '^INSERT.*VALUES\(' > {{ fein_www }}_data/fein-page_page.inserts
#    - onlyif: "/usr/bin/sqlite3 {{ fein_www }}_data/fein16.sqlite '.tables page_page%' | /bin/egrep 'page_page'"


#fein_db_richtextcontent_export:
#  cmd.run:
#    - name: >
#       /usr/bin/sqlite3 {{ fein_www }}_data/fein16.sqlite ".dump page_page_richtextcontent"
#       | /bin/egrep '^INSERT.*VALUES\(' > {{ fein_www }}_data/fein-page_page_richtextcontent.inserts
#    - onlyif: "/usr/bin/sqlite3 {{ fein_www }}_data/fein16.sqlite '.tables page_page%' | /bin/egrep 'page_page'"


fein_db_sqlite:
  file.managed:
    - name: {{ fein_www }}_data/fein16.sqlite
    - user: root
    - group: {{ wwwgroup }}
    - mode: 664
    - contents: ''
    - replace: False
    # - replace: True would be inappropriate where contents: ''
    - require:
      - file: fein_data_directory


fein_urls_py:
  file.managed:
    - name: {{ fein_www }}/urls.py
    - contents: |
        from django.conf.urls import include, patterns, url
        urlpatterns = patterns('',
          url(r'', include('{{ appname }}.urls')),
        )
    # - source: salt://fein/urls.py
    - backup: minion


fein_app_urls_py:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/urls.py
    - contents: |
        from django.conf.urls import include, patterns, url
        from django.views import generic as views_generic
        from django.views.defaults import page_not_found as view_page_not_found
        from django.views.defaults import server_error as view_server_error
        #from django.conf.urls.i18n import i18n_patterns
        from django.contrib import admin
        from django.contrib.staticfiles.urls import staticfiles_urlpatterns
        import os.path
        from feincms.contrib.preview.views import PreviewHandler

        admin.autodiscover()

        urlpatterns = patterns('',
          url(r'^admin/', include(admin.site.urls)),
          url(r'', include('feincms.contrib.preview.urls')),
          # url(r'^(.*)/_preview/(\d+)/$', PreviewHandler.as_view(), name='feincms_preview'),
          # url(r'', include('feincms.urls')),
          url(r'', include('feincms.views.cbv.urls')),
        ) + staticfiles_urlpatterns()
        urlpatterns += patterns('',
          (r'^about/', views_generic.TemplateView.as_view(template_name="about.html")),
          (r'^401/$', views_generic.TemplateView.as_view(template_name="unauth.html")),
          (r'^404/$', view_page_not_found),
          (r'^500/$', view_server_error),
          (r'^temp2$', views_generic.TemplateView.as_view(template_name="template2.html")),
          (r'^temp9$', view_server_error),
        )
        urlpatterns += patterns('{{ appname }}.views',
          url(r'^temp4$', views_generic.TemplateView.as_view(template_name="template4.html")),
          url(r'^page_counts$', 'page_counts', name='page_counts'),
          url(r'^intruder$', 'jresponse', {'seed': 'intruder'}, name='intruder'),
          url(r'^doris$', 'jresponse', {'seed': 'doris'}, name='doris'),
          url(r'^lettuce$', 'jresponse', {'seed': 'lettuce'}, name='lettuce'),
        )
    # - source: salt://fein/urls.py
    - backup: minion
    - require:
      - file: fein_urls_py
      - file: fein_app_views_py


fein_wsgi_py:
  file.managed:
    - name: {{ fein_www }}/wsgi.py
    - contents: |
        # WSGI config for fein project.
        from os import environ

        environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

        # This application object is used by any WSGI server configured to use this
        # file. This includes Django's development server, if the WSGI_APPLICATION
        # setting points here.
        
        from django.core.wsgi import get_wsgi_application
        application = get_wsgi_application()
    # - source: salt://fein/wsgi.py
    - backup: minion


fein_wsgi_conf:
  file.append:
    - name: /etc/apache2/mods-available/wsgi.conf
    - text: |
        <IfModule mod_wsgi.c>
        WSGIPythonPath {{ fein_www }}
        </IfModule>
    # backup: minion
    - require:
      - pkg: fein_wsgi_module
    - watch_in:
      - service: apache2


#fein_apache_conf_tarrup:
#  cmd.run
#    - name: tar -cf /root/apache2.tar /etc/apache2
#  If you have file.append use or other file functions that do not support paramter backup: minion


fein_wsgi_site_available:
  file.managed:
    - name: /etc/apache2/sites-available/fein
    - require_in:
      - file: /etc/apache2/sites-enabled/fein
    - contents: |
        <VirtualHost *:80>

          DocumentRoot {{ fein_www }}

          <Directory />
             #Options FollowSymLinks MultiViews
             Options MultiViews
             AllowOverride None
          </Directory>

          <Directory "{{ fein_www }}/">
             #Options FollowSymLinks MultiViews
             Options -Indexes
             AllowOverride None
             Order allow,deny
             allow from all
          </Directory>

          WSGIDaemonProcess fein user={{ wwwgroup }} group={{ wwwgroup }} processes=1 threads=2 maximum-requests=10000
          WSGIScriptAlias /cms {{ fein_www }}/wsgi.py
          #WSGIPythonPath {{ fein_www }}/ # wsgi.conf
 
          Alias /st {{ fein_www }}_st/

          <FilesMatch "^.*settings.*\.(bak|conf|py|sed)~?">
            Order deny,allow
            deny from all
            allow from 10.0.0.1/24
            allow from 172.16.0.0/16
          </FilesMatch>

        </VirtualHost>


fein_wsgi_site_enabled:
  file.symlink:
    - name: /etc/apache2/sites-enabled/fein
    - target: /etc/apache2/sites-available/fein
    - force: True
    - watch_in:
      - service: apache2
    - require:
      - file: /etc/apache2/sites-enabled/000-default 
      - pkg: fein_packages
      - pkg: fein_wsgi_module


apache2default_disable:
  cmd.run:
    - name: '/usr/sbin/a2dissite 000-default'
    - require:
      - file: fein_wsgi_site_enabled


apache2default_remove:
  file.absent:
    - name: /etc/apache2/sites-enabled/000-default
    - require:
      - pkg: fein_packages
      #- file: fein_wsgi_site_enabled


fein_startapp:
  cmd.run:
    - name: /usr/bin/django-admin startapp {{ appname }} --pythonpath={{ fein_www }} --settings=settings
    # --noinput is an arg for migrate / syncdb but not startapp
    - cwd: {{ fein_www }}
    - timeout: 40
    - require:
      - pkg: fein_packages
      - pkg: fein_wsgi_module


fein_app_views_py:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/views.py
    - contents: |
        from django.http import HttpResponse
        from django.shortcuts import render
        #import models
        from feincms.module.page import models as page_models
        #from feincms.content.richtext import models as richtext_models 
        #from django.template import RequestContext

        def page_counts_context():
            page_count = 0
            richtextcontent_count = 0
            try:
                page_count = page_models.Page.objects.count()
                #richtextcontent_count = richtext_models.RichTextContent.objects.count()
            except ValueError:
                pass
            context = {'page_count': page_count,
                       'richtext_count': richtextcontent_count}
            return context

        def page_counts(request):
            return render(request,'fein_counts.html',page_counts_context())

        def jresponse(request,seed):
            punchline = "Knock Knock What?"
            if seed == 'intruder':
                punchline = 'Window'
            elif seed == 'doris':
                punchline = "Doris locked, that's why I'm knocking!"
            elif seed == 'lettuce':
                punchline = "Lettuce in and you'll find out!"
            return HttpResponse(punchline)
    - user: root
    - group: {{ wwwgroup }}
    - backup: minion
    - require:
      - cmd: fein_startapp
    - backup: minion


fein_app_models_py:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/models.py
    - contents: |
        from django.utils.translation import ugettext_lazy as _

        from feincms.module.page.models import Page
        from feincms.content.richtext.models import RichTextContent
        from feincms.content.medialibrary.models import MediaFileContent
        #from feincms.content.image.models import ImageContent
        #from feincms.content.raw.models import RawContent
        #from feincms.content.application.models import ApplicationContent

        Page.register_templates({
            'title': _('FeinCMS Template2 Example'),
            'path': 'template2.html',
            'regions': (
                ('header', _('Page header.')),
                ('main', _('Main content area')),
                ('sidebar', _('Sidebar'), 'inherited'),
            ),
        })

        Page.register_templates({
            'title': _('FeinCMS Template4 Example'),
            'path': 'template4.html',
            'regions': (
                ('header', _('Page header.')),
                ('main', _('Main content area')),
                ('sidebar', _('Sidebar'), 'inherited'),
                ('footer', _('Page footer.')),
            ),
        })

        Page.register_templates({
            'key': 'base',
            'title': 'Base Template',
            'path': 'base.html',
            'regions': (
                ('main', 'Main region'),
                ('sidebar', 'Sidebar', 'inherited'),
            ),
        })

        Page.create_content_type(RichTextContent)
        #Page.create_content_type(RawContent)
        MediaFileContent.default_create_content_type(Page)
    - user: root
    - group: {{ wwwgroup }}
    - backup: minion
    - require:
      - cmd: fein_startapp


fein_migrate_sync:
  cmd.run:
    - name: /usr/bin/django-admin syncdb --noinput --pythonpath={{ fein_www }} --settings=settings
    # name: /usr/bin/django-admin syncdb --pythonpath={{ fein_www }} --settings=settings
    # Either use the --noinput flag or Enable fein_createsuperuser
    - cwd: {{ fein_www }}
    - timeout: 120
    - prereq:
      - file: fein_db_sqlite
    - require:
      #- cmd: fein_startapp
      - file: fein_app_models_py


fein_manage_py:
  file.managed:
    - name: {{ fein_www }}/manage.py
    - contents: |
        #!/usr/bin/env python
        import os
        import sys

        if __name__ == "__main__":
            os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

            from django.core.management import execute_from_command_line

            execute_from_command_line(sys.argv)
    - user: root
    - group: {{ wwwgroup }}
    - mode: 750
    - require:
      - file: fein_settings_py


fein_createsuperuser:
  cmd.run:
    - name: >
       printf "from django.contrib.auth.models import User;
       User.objects.create_superuser('{{ adminuser }}',
       'fein16+public@wrightsolutions.co.uk', '{{ adminpassword }}')"
       | python2 ./manage.py shell
    # /usr/bin/django-admin createsuperuser --pythonpath={{ fein_www }} --settings=settings
    # --username={{ adminuser }} --email=fein16+public@wrightsolutions.co.uk
    - cwd: {{ fein_www }}
    - timeout: 40
    - prereq:
      - file: fein_db_sqlite
    - require:
      - file: fein_manage_py
      - cmd: fein_migrate_sync


fein_static_soon:
  file.managed:
    - name: {{ fein_www }}_st/coming_soon.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>Coming soon...</title>
        </head>
        <body>
        <p>Under construction - coming soon.</p>
        </body>
        </html>
    - backup: minion


fein401template:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/templates/unauth.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>No authority</title>
        </head>
        <body>
        <p>Unable to access the requested page - no authority.</p>
        </body>
        <!-- http 401 error -->
        </html>
    - backup: minion


fein404template:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/templates/404.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>Page not found</title>
        </head>
        <body>
        <p>Unable to find the requested page - not found.</p>
        </body>
        <!-- http 404 error -->
        </html>
    # - source: salt://fein/urls.py
    - backup: minion


fein500template:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/templates/500.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>Page error</title>
        </head>
        <body>
        <p>Unable to process the requested page - malfunction.</p>
        </body>
        <!-- http 500 error -->
        </html>
    - backup: minion


fein_about_template:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/templates/about.html
    - contents: |
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <meta name="robots" content="noarchive,follow" />
        <title>About Page - cms</title>
        </head>
        <body>
        <p>cms application from automation</p>
        </body>
        </html>
    - user: root
    - group: {{ wwwgroup }}
    - backup: minion


fein_template2:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/templates/template2.html
    - contents: |
{% raw %}
        <div id="header">
            {% block header %}
            {% for content in feincms_page.content.header %}
                {{ content.render }}
            {% endfor %}
            {% endblock %}
        </div>
        <div id="content">
            {% block content %}
            {% for content in feincms_page.content.main %}
                {{ content.render }}
            {% endfor %}
            {% endblock %}
        </div>
{% endraw %}
    - user: root
    - group: {{ wwwgroup }}
    - backup: minion


fein_template4_copy:
  file.copy:
    - name: {{ fein_www }}/{{ appname }}/templates/template4.html
    - source: {{ fein_www }}/{{ appname }}/templates/template2.html
    - require:
      - file: fein_template2
    - backup: minion


fein_template4:
  file.append:
    - name: {{ fein_www }}/{{ appname }}/templates/template4.html
    - text: |
{% raw %}
        <div id="sidebar">
            {% block sidebar %}
            {% for content in feincms_page.content.sidebar %}
                {{ content.render }}
            {% endfor %}
            {% endblock %}
        </div>
        <div id="footer">
            {% block footer %}
            {% for content in feincms_page.content.footer %}
                {{ content.render }}
            {% endfor %}
            {% endblock %}
        </div>
{% endraw %}
    - require:
      - file: fein_template4_copy
    - backup: minion


fein_counts_template:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/templates/fein_counts.html
    - contents: |
{% raw %}
        {% if page_count > 0 %}
            <p>Page count: {{ page_count }}.</p>
            <p>Richtext content: {{ richtext_count }}.</p>
        {% else %}
            <p>No pages available.</p>
        {% endif %}
{% endraw %}
    - user: root
    - group: {{ wwwgroup }}
    - backup: minion


fein_base_template:
  file.managed:
    - name: {{ fein_www }}/{{ appname }}/templates/base.html
    - contents: |
{% raw %}
        {% load applicationcontent_tags feincms_tags feincms_page_tags %}
        <html>
        <head>
            <title>{{ feincms_page.title }}</title>
            <style type="text/css">
            body {
                font-family: Arial, "Liberation Sans", "Nimbus Sans L", "Droid Sans", Helvetica, Lucida, sans-serif;
                font-size: 12px;
                padding: 0;
                margin: 0;
            }

            h1 {
                background: #f2f2f2;
                border-bottom: 1px solid #ccc;
                color: #20435c;
                padding: 10px 0 10px 10px;
                margin: 0;
            }

            #navigation {
                background: #e2e2e2;
                border-bottom: 1px solid #ccc;
                width: 100%;
                padding: 0 0 0 10px;
            }

            #navigation a {
                float: left;
                display: block;
                padding: 10px 15px;
                margin: 10px 10px 10px 0;
                background: #20435c;
                color: #f2f2f2;
            }

            #navigation a:hover,
            #navigation a.mark {
                background: #40637c;
            }

            #wrapper {
                width: 830px;
            }

            #main {
                float: right;
                width: 500px;
            }

            #sidebar {
                float: left;
                width: 300px;
                background: #f2f2f2;
                border-right: 1px solid #ccc;
                min-height: 300px;
                padding: 0 0 0 10px;
            }


            .clearfix:after {
                content: ".";
                display: block;
                clear: both;
                visibility: hidden;
                line-height: 0;
                height: 0;
            }

            .clearfix {
                display: inline-block;
            }

            html[xmlns] .clearfix {
                display: block;
            }

            * html .clearfix {
                height: 1%;
            }
            </style>
        </head>
        <body>
        <h1>{{ feincms_page.title }}</h1>

        <div id="navigation" class="clearfix">
            {% feincms_nav feincms_page level=1 as toplevel %}
            {% for p in toplevel %}
            <a {% if p|is_equal_or_parent_of:feincms_page %}class="mark"{% endif %} href="{{ p.get_absolute_url }}">{{ p.title }}</a>
            {% endfor %}
        </div>

        <div id="wrapper">
            <div id="main">
                <h2>Main content</h2>
                {% block content %}{% feincms_render_region feincms_page "main" request %}{% endblock %}
            </div>

            <div id="sidebar">
                <h2>Sidebar content</h2>
                {% feincms_nav feincms_page level=2 as sublevel %}
                {% for p in sublevel %}
                    {% if forloop.first %}<ul>{% endif %}
                    <li><a href="{{ p.get_absolute_url }}">{{ p.title }}</a></li>
                    {% if forloop.last %}</ul>{% endif %}
                {% empty %}
                    No subpages
                {% endfor %}

                {% block sidebar %}{% feincms_render_region feincms_page "sidebar" request %}{% endblock %}
            </div>
        </div>

        {% feincms_frontend_editing feincms_page request %}

        </body>
        </html>
{% endraw %}
    - user: root
    - group: {{ wwwgroup }}
    - backup: minion


fein_settings_deny_conf:
  file.managed:
    - name: {{ fein_www }}/settings_deny.conf
    - contents: |
        #<VirtualHost *:80>
          # We have included some context and the VirtualHost enclosure
          # to demonstrate where the FilesMatch might typically sit.
          # Just take the 6 lines starting <FilesMatch into your target conf
          #DocumentRoot /var/www
          <Directory />
            Options FollowSymLinks
            AllowOverride None
          </Directory>
          <FilesMatch "^.*settings.*\.(bak|conf|py|sed)~?">
            Order deny,allow
            deny from all
            allow from 10.0.0.1/24
            allow from 172.16.0.0/16
          </FilesMatch>

          ErrorLog ${APACHE_LOG_DIR}/error.log
        #</VirtualHost>


fein_collectstatic:
  cmd.run:
    - name: /usr/bin/django-admin collectstatic --noinput --pythonpath={{ fein_www }} --settings=settings
    # name: /usr/bin/django-admin collectstatic --pythonpath={{ fein_www }} --settings=settings
    - cwd: {{ fein_www }}
    - timeout: 120
    - require:
      - cmd: fein_startapp
      - file: fein_app_urls_py
      - file: fein_local_settings_py


#fein_richtext_tinymce_dir:
  # FEINCMS_RICHTEXT_INIT_CONTEXT = { 'TINYMCE_JS_URL': STATIC_URL + 'www/tiny_mce.js', }
  # Use above instead of this file.directory block
#  file.directory:
#    - name: {{ fein_www }}/media/js
#    - user: root
#    - group: {{ wwwgroup }}
#    - mode: 2755
#    - clean: False
#    - exclude_pat: '*.js'
#    - makedirs: True
#    - require:
#      - file: fein_www_directory


fein_examples_all:
  hg.latest:
    - name: https://bitbucket.org/wrightsolutions/fein1example
    # - rev: 2b9faa9f33994fb1130f5386eb9eb2f685b5b0a5
    - target: {{ fein_www }}/fein1example
    - require:
      - pkg: fein_packages


fein_scripts_directory:
  file.directory:
    - name: {{ fein_www }}_scripts
    - user: root
    - group: root
    # group: {{ wwwgroup }}
    - mode: 2750
    - clean: True
    # exclude_pat: '*.sh'


fein_local_database_repo:
  hg.latest:
    - name: https://bitbucket.org/wrightsolutions/local-database
    - rev: e067d3c82a853c9a18cf241ebdded7e54f99e570
    - target: {{ fein_www }}_scripts/local-database
    - require:
      - pkg: fein_packages


fein_local_webserver_repo:
  hg.latest:
    - name: https://bitbucket.org/wrightsolutions/local-webserver
    - rev: 8647d7b821646a52c526e6096ccdf12588ef0280
    - target: {{ fein_www }}_scripts/local-webserver
    - require:
      - pkg: fein_packages


fein_superuser_minimum1:
  cmd.run:
    - name: >
       printf "from django.contrib.auth.models import User;
       User.objects.create_superuser('{{ adminuser }}',
       'fein16+public@wrightsolutions.co.uk', '{{ adminpassword }}')"
       | python2 ./manage.py shell
    # /usr/bin/django-admin createsuperuser --pythonpath={{ fein_www }} --settings=settings
    # --username={{ adminuser }} --email=fein16+public@wrightsolutions.co.uk
    - cwd: {{ fein_www }}
    - timeout: 40
    - unless: >
       python2 {{ fein_www }}/local-database/sqlite_table_exists_minimum_count.py
       {{ fein_www }}_data/fein16.sqlite 'auth_user' 1
    - require:
      - file: fein_manage_py
      - cmd: fein_migrate_sync
    - order: -2


fein_apache_graceful:
  cmd.run:
    - names:
      - /usr/bin/find {{ fein_www }}/ -xdev -type f -name '*.pyc' -exec rm -f {} \;
      - /usr/sbin/apache2ctl -k graceful
    # Only use multiple entries in names of cmd.run when ok to be run in any order
    - onlyif: /usr/sbin/apache2ctl configtest
    - order: last

