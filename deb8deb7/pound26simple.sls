{% set chroot_parent = salt['pillar.get']('chroot_parent','/var/chroot') %}
{% set chroot_tempdir = '/root/pound_chroot_automation' %}
{% set pound_group = salt['pillar.get']('pound_group','nogroup') %}
pound_packages:
  pkg.installed:
    - pkgs:
      - pound

#    - name: {{ salt['pillar.get']('management:apache', 'salt') }}

#pound_index_html:
#  file.managed:
#    - name: /var/www/html/index.html
#    - source: salt://pound/index.html
# Pound NEVER serves.


pound_chroot_config:
  file.copy:
    - name: /etc/pound/pound_chroot.cfg
    - source: /etc/pound/pound.cfg
    - force: true
    - require:
      - pkg: pound_packages
    - backup: minion


pound_chroot_config_timeout:
  file.replace:
    - name: /etc/pound/pound_chroot.cfg
    - flags:
      # - 'IGNORECASE'
      - 'DOTALL'
      # - 'DEBUG'
    - pattern: "## use hardware-accelleration card supported by openssl.*1.*:"
    - repl: |
        # Seconds rather than microseconds please!
        Client       10
        #TimeOut      15		
        TimeOut      10
        Grace        10

        # use hardware-accelleration card supported by openssl(1):
    - count: 1
    - show_changes: true


make_chroot_pound_tempdir:
  file.directory:
    - name: {{ chroot_tempdir }}/
    - user: root
    - group: {{ pound_group }}
    - mode: 0750
    - clean: True
    # exclude_pat: '*.sh'


make_chroot_scripts_pound:
  hg.latest:
    - name: https://bitbucket.org/wrightsolutions/local-bin
    - rev: c1c0d33cd1747e49349491d2993c0c3c48610466
    - target: {{ chroot_tempdir }}/local-bin
    - require:
      - file: make_chroot_pound_tempdir


chroot_parent_pound:
  file.directory:
    - name: {{ chroot_parent }}/
    - user: root
    - group: {{ pound_group }}
    - mode: 0750
    - clean: False
    # exclude_pat: '*.sh'


make_chroot_pound:
  cmd.run:
    - name: ./make_chroot.sh pound {{ chroot_parent }} 'Y'
    - cwd: {{ chroot_tempdir }}/local-bin
    - timeout: 60
    - require:
       # file: {{ chroot_parent }}/
       - file: chroot_parent_pound

