# encfs on Debian 8 jessie
# Notes about id fields and wisdom (or otherwise) of using filepaths as id
# ID: Must be unique across entire state tree.
#     If the same ID declaration is used twice,
#     only the first one matched will be used...
#     ... All subsequent ID declarations with the same name will be ignored
# YAML and repeated keys:
#   dictionary keys need to be unique so within a given block if you redefine a key, then the last one wins out
# YAML lists and ordering:
#   YAML lists should preserve order, but how the consumer application (Salt) deals with that for cmd.run names
#   should be verified if you are planning to run multiline cmd using  '- names:' form of cmd.run
#

modinfo_packages:
  pkg.installed:
    - pkgs:
      - kmod


encfs_packages:
  pkg.installed:
    - pkgs:
      - ecryptfs-utils
      - encfs
      - libpam-encfs
      - libpam-mount


encfs_modinfo:
  cmd.run:
    - names:
      - /sbin/modinfo ecryptfs
      - /sbin/lsmod
    # Only use multiple entries in names of cmd.run when ok to be run in any order
    - timeout: 30
    - require:
      - pkg: modinfo_packages
      - pkg: encfs_packages


#encfs_modules_conf_append:
#  file.append:
#    - name: /etc/modules-load.d/modules.conf
#    - text: |
#        ecryptfs
#    # - source: salt://fein/modules.conf
#    - backup: minion
#    - require:
#      - pkg: encfs_packages


encfs_modules_conf:
  file.managed:
    - name: /etc/modules-load.d/modules.conf
    - contents: |
        # /etc/modules: kernel modules to load at boot time.
        #
        # This file contains the names of kernel modules that should be loaded
        # at boot time, one per line. Lines beginning with "#" are ignored.

        ecryptfs
    # - source: salt://fein/modules.conf
    - backup: minion
    - require:
      - pkg: encfs_packages


encfs_homes_list:
  cmd.run:
    - name: >
       find /home/.encfs/ -type f
       -name '.encfs6.xml' -mtime -7 -ls
    - timeout: 40
    #- prereq:
    #  - file: fein_db_sqlite
    - require:
      - pkg: encfs_packages


encfs_mount_verify:
  cmd.run:
    - names:
      - /bin/mount
      - /usr/bin/whoami
    # Only use multiple entries in names of cmd.run when ok to be run in any order
    - onlyif: /bin/fgrep -qs 'fuse.encfs' /proc/mounts
    - order: last

