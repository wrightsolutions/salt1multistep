# Setup a cron so that croncmd is executed at midnight give or take a minute
{% set croncmd = salt['pillar.get']('croncmd','/sbin/shutdown -h +1') %}
{% set crontab_user = 'root' %}

input_crontab:
  file.managed:
    - name: /root/input.crontab
    - contents: |
        # m h  dom mon dow   command
	59 23 * * * {{ croncmd }}
    # Below with user name is more suitable for if placing in /etc/cron.d
    #	59 23 * * * root {{ croncmd }}
    # - source: salt://crontab/midnight.crontab
    - backup: minion
    - onlyif: /bin/run-parts --list --regex '^cron$' /etc/init.d
    - order: 1

root_crontab:
  cmd.run:
    - name: >
       cat /root/input.crontab
       | crontab -
    - require:
      - file: fein_wsgi_site_enabled
    - onlyif: /bin/fgrep 'dom mon' /root/input.crontab
    - order: last

