# Ensure apache2-utils (Debianlike) or httpd-utils (CentOS) is installed
# When variable apache is 'salt' ensure web server is running
apache:
  pkg.installed:
    - pkgs:
      - {{ salt['pillar.get']('apache2','apache2') }}-utils
{% if salt['pillar.get']('apache') == 'salt' %}
      - {{ pillar['apache2'] }}
  service:
    - name: {{ salt['pillar.get']('apache2','apache2') }}
    - running
    - enable: True
{% endif %}
